# aistudio-wpf-aclient-release

#### 介绍
本版本为开源版本[https://gitee.com/akwkevin/aistudio.-wpf.-aclient](https://gitee.com/akwkevin/aistudio.-wpf.-aclient)的Plus版，请运行根目录publish下的AIStudio.Wpf.Client.exe，查看效果。

#### AIStudio框架汇总：[https://gitee.com/akwkevin/aistudio.-introduce](https://gitee.com/akwkevin/aistudio.-introduce)

#### 软件截图

![alt text](client-vip.png)
![alt text](image.png)
![alt text](image-1.png)
![alt text](image-2.png)
![alt text](image-3.png)
![alt text](image-4.png)
![alt text](image-5.png)
![alt text](image-6.png)